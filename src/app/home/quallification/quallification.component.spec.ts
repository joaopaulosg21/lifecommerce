import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuallificationComponent } from './quallification.component';

describe('QuallificationComponent', () => {
  let component: QuallificationComponent;
  let fixture: ComponentFixture<QuallificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuallificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuallificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
