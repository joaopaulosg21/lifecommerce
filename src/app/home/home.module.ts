import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { QuoteService } from './quote.service';
import { QuallificationComponent } from './quallification/quallification.component';
import { RankingComponent } from './ranking/ranking.component';
import { StatusComponent } from './status/status.component';
import { EarningsComponent } from './earnings/earnings.component';
import { NewsComponent } from './news/news.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    QuallificationComponent,
    RankingComponent,
    StatusComponent,
    EarningsComponent,
    NewsComponent
  ],
  providers: [
    QuoteService
  ]
})
export class HomeModule { }
