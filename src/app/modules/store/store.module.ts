import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreRoutingModule } from './store-routing.module';
import { ProductsComponent } from './products/products.component';
import { MaterialModule } from '@app/material.module';
import { FlexLayoutModule } from '../../../../node_modules/@angular/flex-layout';
import { ProductItemComponent } from './product-item/product-item.component';
import { ProductFilterComponent } from './product-filter/product-filter.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderItemComponent } from './order-item/order-item.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    StoreRoutingModule
  ],
  declarations: [
    ProductsComponent,
    ProductItemComponent,
    ProductFilterComponent,
    OrdersComponent,
    OrderItemComponent
  ]
})
export class StoreModule { }
