import { Injectable } from '@angular/core';
import { ProfileModule } from '@app/modules/profile/profile.module';
import { HttpClient } from '@angular/common/http';
import { User } from '@app/shared/models/user.model';

@Injectable({
  providedIn: ProfileModule
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(): Promise<User[]> {
    return this.http.get('user').toPromise()
      .then(data => {
        return data as User[];
      });
  }
}
