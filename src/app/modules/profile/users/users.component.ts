import { Component, OnInit } from '@angular/core';
import { User } from '@app/shared/models/user.model';
import { UsersService } from '@app/modules/profile/users/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: User[];

  constructor(private service: UsersService) { }

  ngOnInit() {
    this.service.getUsers().then(data => this.users = data);
  }

}
