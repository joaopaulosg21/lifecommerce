import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TermsComponent } from './terms/terms.component';
import { DocumentsComponent } from './documents/documents.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from '@app/modules/profile/users/users.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    MaterialModule,
    FlexLayoutModule,
    ProfileRoutingModule
  ],
  declarations: [ProfileComponent, TermsComponent, DocumentsComponent, UsersComponent],
  providers: [
    UsersService
  ]
})
export class ProfileModule { }
