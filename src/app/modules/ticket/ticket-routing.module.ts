import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OpenComponent } from '@app/modules/ticket/open/open.component';
import { TicketListComponent } from '@app/modules/ticket/ticket-list/ticket-list.component';

const routes: Routes = [
  { path: 'open', component: OpenComponent },
  { path: 'list', component: TicketListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketRoutingModule { }
