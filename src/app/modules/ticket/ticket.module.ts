import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketRoutingModule } from './ticket-routing.module';
import { OpenComponent } from './open/open.component';
import { MaterialModule } from '@app/material.module';
import { FlexLayoutModule } from '../../../../node_modules/@angular/flex-layout';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    TicketRoutingModule
  ],
  declarations: [OpenComponent, TicketListComponent, TicketDetailComponent]
})
export class TicketModule { }
