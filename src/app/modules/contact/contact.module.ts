import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { FaqComponent } from './faq/faq.component';
import { MaterialModule } from '@app/material.module';
import { FlexLayoutModule } from '../../../../node_modules/@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ContactRoutingModule
  ],
  declarations: [FaqComponent]
})
export class ContactModule { }
