import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualIdentityComponent } from './visual-identity.component';

describe('VisualIdentityComponent', () => {
  let component: VisualIdentityComponent;
  let fixture: ComponentFixture<VisualIdentityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualIdentityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualIdentityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
