import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeveralComponent } from './several.component';

describe('SeveralComponent', () => {
  let component: SeveralComponent;
  let fixture: ComponentFixture<SeveralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeveralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeveralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
