import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialsRoutingModule } from './materials-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { MaterialsComponent } from './materials.component';
import { DocumentsComponent } from './documents/documents.component';
import { VisualIdentityComponent } from './visual-identity/visual-identity.component';
import { SeveralComponent } from './several/several.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    MaterialsRoutingModule
  ],
  declarations: [
    MaterialsComponent,
    DocumentsComponent,
    VisualIdentityComponent,
    SeveralComponent
  ]
})
export class MaterialsModule { }
