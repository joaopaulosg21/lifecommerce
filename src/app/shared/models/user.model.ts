import { Address } from '@app/shared/models/address.model';

export interface User {
  name: string;
  email: string;
  user: string;
  birthday: string;
  ssn: string;
  phone: string;
  voucher: string;
  country: number;
  address: Address;
}
