export interface Address {
  address: string;
  number: string;
  district: string;
  city: string;
  zip_code: string;
  complement: string;
}
