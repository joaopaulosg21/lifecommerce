import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { LoaderComponent } from './loader/loader.component';
import { NgxMaskModule } from 'ngx-mask';
import { NgxsModule } from '@ngxs/store';

@NgModule({
  imports: [
    FlexLayoutModule,
    MaterialModule,
    NgxMaskModule.forRoot(),
    CommonModule
  ],
  declarations: [
    LoaderComponent
  ],
  exports: [
    LoaderComponent,
    NgxMaskModule
  ]
})
export class SharedModule { }
